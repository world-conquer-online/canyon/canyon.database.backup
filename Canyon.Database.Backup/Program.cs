﻿using Canyon.Database.Backup.Threads;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;

namespace Canyon.Database.Backup
{
    internal class Program
    {
        public static ServerConfiguration ServerConfiguration;

        public static async Task Main(params string[] args)
        {
            ServerConfiguration = new ServerConfiguration(args);

            using IHost host = Host.CreateDefaultBuilder(args)
                .ConfigureLogging(logging =>
                {
                    logging.AddSimpleConsole(x =>
                    {
                        x.SingleLine = true;
                        x.TimestampFormat = "[yyyyMMdd HHmmss.fff] ";
                    });
                    logging.AddFile("Logs/CanyonDatabaseBackup-{Date}.log");
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddQuartz(q =>
                    {
                        q.UseMicrosoftDependencyInjectionJobFactory();

                        JobKey key = new("AutomaticBackupJob");
                        q.AddJob<MySQLDumpThread>(key);

                        q.AddTrigger(opts => opts
                            .ForJob(key)
                            .WithIdentity("AutomaticBackupJob-Trigger")
#if DEBUG
                            .WithSchedule(SimpleScheduleBuilder.RepeatMinutelyForever())
#else
                            .WithCronSchedule("0 0 */6 * * ?")
#endif
                        );
                    });

                    // Add the Quartz.NET hosted service
                    services.AddQuartzHostedService(q =>
                    {
                        q.WaitForJobsToComplete = true;
                    });
                })
                .Build();

            await host.RunAsync();
        }
    }
}