﻿using FluentFTP;
using FluentFTP.Logging;
using Microsoft.Extensions.Logging;
using Quartz;
using SharpCompress.Archives.GZip;
using System.Diagnostics;

namespace Canyon.Database.Backup.Threads
{
    [DisallowConcurrentExecution]
    public sealed class MySQLDumpThread : IJob
    {
        private readonly ILogger<MySQLDumpThread> logger;

        public MySQLDumpThread(ILogger<MySQLDumpThread> logger)
        {
            this.logger = logger;
        }

        public Task Execute(IJobExecutionContext context)
        {
            logger.LogInformation("Starting dump process");
            try 
            {
                if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "Output")))
                {
                    Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "Output"));
                }

                DoBackup();
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "An critical error ocurred while dumping the database. Message: {}", ex.Message);
                logger.LogCritical("The job stopped with an error");
            }
            return Task.CompletedTask;
        }

        private void DoBackup()
        {
            string mysqlDump = Program.ServerConfiguration.MySQLDumpPath;
            logger.LogInformation($"Checking if file exists: {mysqlDump}");
            if (!File.Exists(mysqlDump))
            {
                throw new FileNotFoundException("Could not find file.", mysqlDump);
            }

            // --host=localhost --port=3306 --default-character-set=utf8 --user=root --protocol=tcp --order-by-primary=TRUE --single-transaction=TRUE --column-statistics=0 --skip-triggers "cq"
            string outputFile = Path.Combine(Environment.CurrentDirectory, "Output", $"dump-{DateTime.Now:yyyyMMddHHmmss}.sql");
            var databaseSettings = Program.ServerConfiguration.Database;
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = mysqlDump,
                    Arguments = $"--host={databaseSettings.Hostname} --port={databaseSettings.Port} --default-character-set=utf8mb4 --user={databaseSettings.Username} -p\"{databaseSettings.Password}\" " +
                                $"--protocol=tcp --order-by-primary=TRUE --single-transaction=TRUE --skip-lock-tables --lock-tables=FALSE --skip-triggers \"{databaseSettings.Schema}\" -r \"{outputFile}\"",
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                }
            };

            process.ErrorDataReceived += (sender, e) =>
            {
                if (!string.IsNullOrEmpty(e.Data))
                {
                    logger.LogError($"{e.Data}");
                }
            };
            process.OutputDataReceived += (sender, e) =>
            {
                if (!string.IsNullOrEmpty(e.Data))
                {
                    logger.LogInformation($"{e.Data}");
                }
            };

            process.Start();
            process.BeginErrorReadLine();
            process.BeginOutputReadLine();
            process.WaitForExit();
            process.Close();

            logger.LogInformation("Finished dump process!");
            logger.LogWarning("Starting GZIPING process!! Do not close application!!!!");

            // I know MySQL offers this option, but I wanted to test this lib
            using var archive = GZipArchive.Create();
            using MemoryStream memoryStream = new MemoryStream(File.ReadAllBytes(outputFile));
            archive.AddEntry(Path.GetFileName(outputFile), memoryStream);
            string gzippedFile = Path.Combine(Environment.CurrentDirectory, "Output", $"{Path.GetFileNameWithoutExtension(outputFile)}.gz");
            archive.SaveTo(gzippedFile);

            File.Delete(outputFile);

            logger.LogInformation("GZIPING complete!");
            logger.LogInformation($"Backup saved to \"{gzippedFile}\"");

            var ftpSettings = Program.ServerConfiguration.Ftp ?? new ServerConfiguration.FtpConfiguration();
            if (!string.IsNullOrEmpty(ftpSettings.Hostname)
                && !string.IsNullOrEmpty(ftpSettings.Username)
                && !string.IsNullOrEmpty(ftpSettings.Password))
            {
                logger.LogInformation("Starting FTP upload");

                using var ftp = new FtpClient(ftpSettings.Hostname, ftpSettings.Username, ftpSettings.Password, 0, null, new FtpLogAdapter(logger));
                ftp.Connect();
                ftp.UploadFile(gzippedFile, Path.GetFileName(gzippedFile), FtpRemoteExists.Overwrite, true, FtpVerify.Retry);

                const int backupDays = 14;
                const int backupsPerDay = 4;
                const int filesToKeep = backupDays * backupsPerDay;
                
                logger.LogInformation("Accessing FTP to delete files older than {} days", backupDays);

                FtpListItem[] fileList = ftp.GetListing();
                foreach (var deleteFile in fileList
                    .OrderByDescending(x => x.RawModified)
                    .Skip(filesToKeep))
                {
                    logger.LogWarning($"Deleting backup \"{deleteFile.FullName}\" from {deleteFile.RawModified}");
                    ftp.DeleteFile(deleteFile.FullName);
                }

                logger.LogInformation("Cleaning complete!");

                ftp.Disconnect();
            }
            else
            {
                logger.LogInformation("No FTP settings set! Skipping FTP upload");
            }

            logger.LogInformation("Process finished!");
        }
    }
}
