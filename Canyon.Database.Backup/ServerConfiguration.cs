﻿using Microsoft.Extensions.Configuration;

namespace Canyon.Database.Backup
{
    public class ServerConfiguration
    {
        public ServerConfiguration(params string[] args)
        {
            new ConfigurationBuilder()
                .AddJsonFile("Canyon.Backup.Config.json")
                .AddCommandLine(args)
                .AddEnvironmentVariables()
                .Build()
                .Bind(this);
        }

        public string MySQLDumpPath { get; set; }
        public DatabaseConfiguration Database { get; set; }
        public FtpConfiguration Ftp { get; set; }
        public List<string> Tables { get; set; }

        public class DatabaseConfiguration
        {
            public string Schema { get; set; }
            public string Hostname { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public int Port { get; set; }
        }

        public class FtpConfiguration
        {
            public string Hostname { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public int Port { get; set; }
        }
    }
}
